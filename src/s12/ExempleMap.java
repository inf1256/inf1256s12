/**
 * @author Johnny Tsheke @ UQAM -- INF1256
 * 2017-03-27
 */
package s12;

import java.util.*;
public class ExempleMap {
	public static void main(String[] args) {
		Map<Integer,String> map = new HashMap<Integer,String>();
        map.put(12,"Marcel");
        map.put(45,"Tremblay");
        map.put(27,"David");
        System.out.println("ens a " + map.size() +" eléments");
        map.put(12,"Marc");//cled deja utilise, remplace valeur
        System.out.println("ens a " + map.size() +" eléments");
        map.put(200,"Charles");
        System.out.println("ens a " + map.size() +" eléments");    
        for(String nom:map.values()){
        	System.out.println("Avec for et .values() : "+nom);
        }
   
        map.forEach((k,v)->System.out.println("Avec forEach: Clé= "+k+" valeur= "+v));
        for(Iterator<Integer> it= map.keySet().iterator();it.hasNext();){
        	Integer cle = it.next();
        	String valeur = map.get(cle);
        	System.out.println("Avec iterateur: Cle= "+cle+" valeur= "+valeur);
        }
	}
}
