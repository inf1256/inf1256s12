/**
 * @author Johnny Tsheke @ UQAM -- INF1256
 * 2017-03-27
 */
package s12;


public class Tableau {

	public static void main(String[] args) {
		int[] tab = {1, 4, 3};
		int[][] tab2 = {{1, 4, 3}, {4, 5, 6}};
		System.out.println("Element indice 1 tab = "+ tab[1]);
		System.out.println("Nombre élements dans tab = "+tab.length);
		System.out.println("Element coordonnées (1,2) de tab2 = "+tab2[1][2] );
		System.out.println("Nombre sous tableaux dans tab2 = "+tab2.length);		

	}

}
