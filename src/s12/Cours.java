package s12;

public class Cours {
    private String sigle = "";
    private String titre = "";

    public Cours(String sigleCours, String titreCours){ //constructeur
        this.sigle = sigleCours;
        this.titre = titreCours;
    }

    public void affiche(){
        System.out.println(this.sigle + " -- " + this.titre);
    }
}
