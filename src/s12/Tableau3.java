/**
 * @author Johnny Tsheke @ UQAM -- INF1256
 * 2017-03-27
 */
package s12;


public class Tableau3 {

	public static void main(String[] args) {
		int[] tab = {1, 4, 3};
		int[][] tab2 = {{1, 4, 3}, {4, 5, 6}};
		int sommeTab=0;
		int sommeTab2 = 0;
		for (int i=0;i<tab.length;i++){//for simple
			sommeTab = sommeTab +tab[i];
		}
		for(int i = 0; i<tab2.length;i++){//for imbriqués
			for(int j=0;j<tab2[i].length;j++){
				sommeTab2 = sommeTab2 + tab2[i][j];
			}
		}
		
		System.out.println("Somme des éléments tab = "+ sommeTab);
		System.out.println("Somme des éléments tab2 = "+ sommeTab2);

	}

}
