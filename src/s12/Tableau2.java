/**
 * @author Johnny Tsheke @ UQAM -- INF1256
 * 2017-03-27
 */
package s12;


public class Tableau2 {

	public static void main(String[] args) {
		int[] tab = new int[10];//chaque élément initialisé à 0 par défaut
		System.out.println("Nombre élements dans tab = "+tab.length);
		tab[0] = 1;
		tab[1] = 4;
		System.out.println("Element indice 9 tab = "+ tab[9]);
		System.out.println("Nombre élements dans tab = "+tab.length);
		tab[9] = 3;
		System.out.println("Element indice 9 tab = "+ tab[9]);
		System.out.println("Nombre élements dans tab = "+tab.length);
		
		//tab[10] = 7; //indice 10 est hors tableau
				

	}

}
