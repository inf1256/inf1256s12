package s12;

import java.util.ArrayList;
import java.util.Comparator;

public class CoursOfferts {
    ArrayList<Cours> coursDeLaSession;

    public CoursOfferts(){ // constructeur
        coursDeLaSession = new ArrayList<Cours>();
    }

    public static void main(String[] args){
        CoursOfferts coursOfferts = new CoursOfferts();
        Cours cours1 = new Cours("INF1256", "Informatique pour les sciences de la gestion");
        coursOfferts.coursDeLaSession.add(cours1);
        Cours cours2 = new Cours("INF2005", "Programmation Web");
        coursOfferts.coursDeLaSession.add(cours2);
        Cours cours3 = new Cours("INF3005", "Programmation Web avancée");
        coursOfferts.coursDeLaSession.add(cours3);
        System.out.println("Voici les cours offerts cette session : ");
        for(Cours c:coursOfferts.coursDeLaSession){
           c.affiche();
        }
        System.out.println("Autre maniere de parcourir un ArrayList -- forEach");
        coursOfferts.coursDeLaSession.forEach(c->c.affiche());
    }
}
