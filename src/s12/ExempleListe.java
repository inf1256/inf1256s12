/**
 * @author Johnny Tsheke @ UQAM -- INF1256
 * 2017-03-27
 */
package s12;

import java.util.ArrayList;

public class ExempleListe {

	public static void main(String[] args) {
		ArrayList<String> liste = new ArrayList<String>();
        liste.add("Marcel");
        liste.add("Tremblay");
        liste.add("David");
        System.out.println("ens a " + liste.size() +" eléments");
        liste.add("Marcel");//doublon accepte
        System.out.println("liste a " + liste.size() +" eléments");
        liste.add("Charles");
        System.out.println("liste a " + liste.size() +" eléments");
        
        for(String nom:liste){
        	System.out.println(nom);
        }
        liste.add(0,"Georges");// inserer au debut
        liste.forEach(elem->System.out.println("impression avec foreach: "+elem));
      String[] tab = new String[liste.size()];//initialisation tab
    		  liste.toArray(tab);//ATT: toArray() sans arg. retourne un tab  Objects
    		  System.out.println("deuxieme nom est: "+ tab[1]);
	}
}
