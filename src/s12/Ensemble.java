/**
 * @author Johnny Tsheke @ UQAM -- INF1256
 * 2017-03-27
 */
package s12;

import java.util.*;
public class Ensemble {

	public static void main(String[] args) {
		Set<String> ens = new HashSet<String>();
        ens.add("Marcel");
        ens.add("Tremblay");
        ens.add("David");
        System.out.println("ens a " + ens.size() +" eléments");
        ens.add("Marcel");//doublon mais garde une seule occurence
        System.out.println("ens a " + ens.size() +" eléments");
        ens.add("Charles");
        System.out.println("ens a " + ens.size() +" eléments");
        
        for(String nom:ens){
        	System.out.println(nom);
        }
        ens.forEach(elem->System.out.println("impression avec foreach: "+elem));
      String[] tab = new String[ens.size()];//initialisation tab 
    		  ens.toArray(tab);//ATT: toArray() sans arg. retourne un tab  Objects
    		  System.out.println("deuxieme nom est: "+ tab[1]);
	}
}
